Template.main.helpers({
    
    resultFields: function () {

        if (this.first) {
            return _.without(_.keys(this.first), "_id", "insertedAt", "url");
        }
    },
    adminMode: function() {
        return (this.query.mode === 'admin');
    }
    
});

Template.main.events({
   'click #reset': function(e, template) {
       Meteor.call('reset', function (error, results) {
           //alert("Reset by " + results.ip);
       });
   }
});

Template.result.helpers({
    
    values: function() {
                
        return _.map(_.values(_.omit(this, "_id", "insertedAt", "url")), function(item) {
            if ( (new Date(item) !== "Invalid Date" && !isNaN(new Date(item)) )) {
                var temp = new Date(item);
                //return temp.getFullYear() + "/" + temp.getMonth() + "/" +temp.getDay()+1 + " " + temp.getHours() + ":" + temp.getMinutes();
                return temp.toLocaleDateString() + " " + temp.toLocaleTimeString();
            }
            else return item;
        });
    },
    
    highlight: function () {
        var insertedAt = this.insertedAt;
        var now = new Date();

        return (insertedAt.getTime() + 1000 > now.getTime()) ? "highlight" : "";
    }

});

Template.result.events({
    'click tr': function (e, template) {
        window.open(this.url, '_blank');
    }
});