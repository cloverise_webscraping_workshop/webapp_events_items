Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: 'notFound'
});

Router.route('/', {
    name: "main",
    waitOn: function () {
        return Meteor.subscribe("results");
    },
    data: function () {
        
        return {
            query: this.params.query,
            first: Results.findOne({}, {sort: {insertedAt: -1}}),
            results: Results.find({}, {sort: {insertedAt: -1}})
        };
    }
});


Router.route('/api/post_result', {
    name: 'postResult',
    where: 'server',
    action: function () {
        // GET, POST, PUT, DELETE
        var requestMethod = this.request.method;
        // Data from a POST request
        var requestData = this.request.body;

        console.log(requestMethod);

        console.log(eval(requestData));
        
        var resultRecord = _.extend(eval(requestData), {
            "insertedAt": new Date()
        });
        
        var resultId = Results.insert(resultRecord);

        if (!resultId)
            console.log("Failed inserting the result");
        else
            console.log("New result inserted"+resultId);

        // Could be, e.g. application/xml, etc.
        this.response.writeHead(200, {'Content-Type': 'text/html'});
        this.response.end('New result inserted successfully: ' + resultId);
    }
});


